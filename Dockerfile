FROM nodesource/nsolid:erbium-alpine-3.8.0
MAINTAINER freightcorp

# setup app dir
RUN mkdir -p /www/
WORKDIR /www/

# install dependencies
COPY ./package.json /www/package.json
RUN npm install

# copy over app dir
COPY ./ /www/

# run tests
RUN npm test

# start server
CMD npm start

# expose server
EXPOSE 9000
